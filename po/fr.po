# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
#
# Translators:
# Anthony Jorion <pingax@frugalware.org>, 2013-2015
# Anthony Jorion <pingax@frugalware.org>, 2013
# Julien Sansous <bambou_puravida@hotmail.com>, 2012
# Julien Sansous <bambou_puravida@hotmail.com>, 2012
# Anthony Jorion <pingax@frugalware.org>, 2011
# raphaelh <raphael.huck@gmail.com>, 2011
msgid ""
msgstr ""
"Project-Id-Version: Pragha\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-02-26 10:11-0300\n"
"PO-Revision-Date: 2015-02-16 22:10+0000\n"
"Last-Translator: Anthony Jorion <pingax@frugalware.org>\n"
"Language-Team: French (http://www.transifex.com/p/Pragha/language/fr/)\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"

#. GtkInfoBar has undocumented behavior for GTK_RESPONSE_CANCEL
#: ../src/info-bar-import-music.c:80 ../src/info-bar-import-music.c:127
#: ../src/info-bar-import-music.c:170
msgid "_No"
msgstr "_Non"

#: ../src/info-bar-import-music.c:81 ../src/info-bar-import-music.c:128
#: ../src/info-bar-import-music.c:171
msgid "_Yes"
msgstr "_Oui"

#: ../src/info-bar-import-music.c:83
#, c-format
msgid "Would you like to import %s to library?"
msgstr "Voulez-vous importer %s à la bibliothèque ?"

#: ../src/info-bar-import-music.c:130
msgid "Would you like to update your music library?"
msgstr "Voulez-vous mettre à jour votre librairie musicale ?"

#: ../src/info-bar-import-music.c:173
msgid "Some changes need restart pragha."
msgstr "Certains changements nécessitent de redémarrer pragha."

#: ../src/pragha-cmdline.c:192
msgid "FILENAME"
msgstr "NOM DE FICHIER"

#: ../src/pragha-cmdline.c:251
msgid "Use --help to see a full list of available command line options."
msgstr ""
"Utilisez --help pour obtenir une liste complète des options disponibles de "
"la ligne de commande."

#: ../src/pragha-equalizer-dialog.c:50
msgid "Disabled"
msgstr "Désactivé"

#: ../src/pragha-equalizer-dialog.c:51
msgid "Classical"
msgstr "Classique"

#: ../src/pragha-equalizer-dialog.c:52
msgid "Club"
msgstr "Club"

#: ../src/pragha-equalizer-dialog.c:53
msgid "Dance"
msgstr "Dance"

#: ../src/pragha-equalizer-dialog.c:54
msgid "Full Bass"
msgstr "Grave"

#: ../src/pragha-equalizer-dialog.c:55
msgid "Full Bass and Treble"
msgstr "Grave et aigus"

#: ../src/pragha-equalizer-dialog.c:56
msgid "Full Treble"
msgstr "Aigus"

#: ../src/pragha-equalizer-dialog.c:57
msgid "Laptop Speakers and Headphones"
msgstr "Hauts-parleurs du portable et casque"

#: ../src/pragha-equalizer-dialog.c:58
msgid "Large Hall"
msgstr "Grande salle"

#: ../src/pragha-equalizer-dialog.c:59
msgid "Live"
msgstr "Live"

#: ../src/pragha-equalizer-dialog.c:60
msgid "Party"
msgstr "Fête"

#: ../src/pragha-equalizer-dialog.c:61
msgid "Pop"
msgstr "Pop"

#: ../src/pragha-equalizer-dialog.c:62
msgid "Reggae"
msgstr "Reggae"

#: ../src/pragha-equalizer-dialog.c:63
msgid "Rock"
msgstr "Rock"

#: ../src/pragha-equalizer-dialog.c:64
msgid "Ska"
msgstr "Ska"

#: ../src/pragha-equalizer-dialog.c:65
msgid "Smiley Face Curve"
msgstr ""

#: ../src/pragha-equalizer-dialog.c:66
msgid "Soft"
msgstr "Soft"

#: ../src/pragha-equalizer-dialog.c:67
msgid "Soft Rock"
msgstr "Soft Rock"

#: ../src/pragha-equalizer-dialog.c:68
msgid "Techno"
msgstr "Techno"

#: ../src/pragha-equalizer-dialog.c:69
msgid "Custom"
msgstr "Personnalisé"

#: ../src/pragha-equalizer-dialog.c:419
msgid "Equalizer"
msgstr "Egaliseur"

#: ../src/pragha-filter-dialog.c:218 ../src/pragha-library-pane.c:506
#: ../src/pragha-tags-dialog.c:114 ../plugins/notify/pragha-notify-plugin.c:166
msgid "Unknown Artist"
msgstr "Artiste inconnu"

#: ../src/pragha-filter-dialog.c:219 ../src/pragha-library-pane.c:513
#: ../src/pragha-library-pane.c:518 ../src/pragha-tags-dialog.c:115
#: ../plugins/notify/pragha-notify-plugin.c:167
msgid "Unknown Album"
msgstr "Album inconnu"

#. The search dialog
#: ../src/pragha-filter-dialog.c:369
msgid "Search in playlist"
msgstr "Rechercher dans la liste de lecture."

#: ../src/pragha-filter-dialog.c:372
msgid "_Close"
msgstr "_Fermer"

#: ../src/pragha-filter-dialog.c:375 ../src/pragha-playlist.c:209
msgid "Add to playback queue"
msgstr "Ajouter à la liste de lecture"

#: ../src/pragha-filter-dialog.c:376
msgid "_Jump to"
msgstr "_Aller à"

#: ../src/pragha-library-pane.c:132
msgid "_Skip"
msgstr "Passer"

#: ../src/pragha-library-pane.c:133
msgid "S_kip All"
msgstr "Tout passer"

#: ../src/pragha-library-pane.c:134
msgid "Delete _All"
msgstr "Tout _supprimer"

#: ../src/pragha-library-pane.c:188
msgid "_Expand library"
msgstr "_Maximiser la vue de la bibliothèque"

#: ../src/pragha-library-pane.c:190
msgid "_Collapse library"
msgstr "_Minimiser la vue de la bibliothèque"

#: ../src/pragha-library-pane.c:192 ../src/pragha-library-pane.c:1585
msgid "Folders structure"
msgstr "Structure des dossiers"

#: ../src/pragha-library-pane.c:194 ../src/pragha-library-pane.c:1594
#: ../src/pragha-playlist.c:115 ../src/pragha-playlist.c:3371
#: ../src/pragha-playlist.c:3547 ../src/pragha-statusicon.c:180
#: ../src/pragha-tags-dialog.c:168 ../src/pragha-tags-dialog.c:1082
msgid "Artist"
msgstr "Artiste"

#: ../src/pragha-library-pane.c:196 ../src/pragha-library-pane.c:1603
#: ../src/pragha-playlist.c:116 ../src/pragha-playlist.c:3372
#: ../src/pragha-playlist.c:3548 ../src/pragha-statusicon.c:181
#: ../src/pragha-tags-dialog.c:169 ../src/pragha-tags-dialog.c:1087
msgid "Album"
msgstr "Album"

#: ../src/pragha-library-pane.c:198 ../src/pragha-library-pane.c:1612
#: ../src/pragha-playlist.c:117 ../src/pragha-playlist.c:3373
#: ../src/pragha-playlist.c:3549 ../src/pragha-tags-dialog.c:170
#: ../src/pragha-tags-dialog.c:1092
msgid "Genre"
msgstr "Genre"

#: ../src/pragha-library-pane.c:200 ../src/pragha-library-pane.c:1624
msgid "Artist / Album"
msgstr "Artiste / Album"

#: ../src/pragha-library-pane.c:202 ../src/pragha-library-pane.c:1648
msgid "Genre / Album"
msgstr "Genre / Album"

#: ../src/pragha-library-pane.c:204 ../src/pragha-library-pane.c:1636
msgid "Genre / Artist"
msgstr "Genre / Artiste"

#: ../src/pragha-library-pane.c:206 ../src/pragha-library-pane.c:1663
msgid "Genre / Artist / Album"
msgstr "Genre / Artiste / Album"

#. Playlist and Radio tree
#: ../src/pragha-library-pane.c:242
msgid "_Add to current playlist"
msgstr "_Ajouter la liste de lecture courante"

#: ../src/pragha-library-pane.c:244
msgid "_Replace current playlist"
msgstr "_Remplacer la liste de lecture courante"

#: ../src/pragha-library-pane.c:246
msgid "Replace and _play"
msgstr "Remplacer et _jouer"

#: ../src/pragha-library-pane.c:248 ../src/pragha-playlists-mgmt.c:514
msgid "Rename"
msgstr "Renommer"

#: ../src/pragha-library-pane.c:250
msgid "Delete"
msgstr "Enlever"

#: ../src/pragha-library-pane.c:252 ../src/pragha-menubar.c:222
#: ../src/pragha-menubar.c:227 ../src/pragha-playlists-mgmt.c:1337
#: ../src/pragha-playlists-mgmt.c:1373
msgid "Export"
msgstr "Exporter"

#. Set dialog properties
#: ../src/pragha-library-pane.c:254 ../src/pragha-tags-dialog.c:147
msgid "Edit tags"
msgstr "Modifier les étiquettes"

#: ../src/pragha-library-pane.c:256
msgid "Move to _trash"
msgstr "_Mettre à la corbeille"

#: ../src/pragha-library-pane.c:258
msgid "Delete from library"
msgstr "Enlever de la bibliothèque"

#: ../src/pragha-library-pane.c:511 ../src/pragha-tags-dialog.c:105
msgid "Unknown"
msgstr "Inconnu"

#: ../src/pragha-library-pane.c:523
msgid "Unknown Genre"
msgstr "Genre inconnu"

#: ../src/pragha-library-pane.c:843
msgid "File can't be moved to trash. Delete permanently?"
msgstr ""
"Le fichier ne peut être déplacé vers la corbeille. Le supprimer "
"définitivement ?"

#: ../src/pragha-library-pane.c:844
#, c-format
msgid "The file \"%s\" cannot be moved to the trash. Details: %s"
msgstr "Le fichier \"%s\" ne peut être mis à la corbeille. Détails : %s"

#: ../src/pragha-library-pane.c:852 ../src/pragha-playlists-mgmt.c:85
#: ../src/pragha-playlists-mgmt.c:517 ../src/pragha-playlists-mgmt.c:589
#: ../src/pragha-playlists-mgmt.c:1239 ../src/pragha-preferences-dialog.c:671
#: ../src/pragha-preferences-dialog.c:1276 ../src/pragha-tags-dialog.c:149
#: ../src/pragha.c:209 ../src/pragha.c:408
#: ../plugins/lastfm/pragha-lastfm-plugin.c:956
#: ../plugins/tunein/pragha-tunein-plugin.c:227
msgid "_Cancel"
msgstr "_Annuler"

#: ../src/pragha-library-pane.c:858
msgid "_Delete"
msgstr "_Supprimer"

#: ../src/pragha-library-pane.c:1895 ../src/pragha-library-pane.c:1977
#: ../src/pragha-playlists-mgmt.c:618 ../src/pragha-playlists-mgmt.c:645
#: ../src/pragha.c:306
msgid "Playlists"
msgstr "Listes de lecture"

#: ../src/pragha-library-pane.c:1911 ../src/pragha-library-pane.c:1986
msgid "Radios"
msgstr "Radios"

#: ../src/pragha-library-pane.c:1927 ../src/pragha-preferences-dialog.c:959
#: ../src/pragha-preferences-dialog.c:1283
msgid "Library"
msgstr "Bibliothèque"

#: ../src/pragha-library-pane.c:2484
msgid "Really want to move the files to trash?"
msgstr "Êtes-vous sûr de mettre les fichiers à la corbeille ?"

#: ../src/pragha-library-pane.c:2486
msgid "Delete permanently instead of moving to trash"
msgstr ""
"Supprimer de manière permanente au lieu de les déplacer vers la corbeille"

#: ../src/pragha-library-pane.c:2536
msgid ""
"Are you sure you want to delete current file from library?\n"
"\n"
"Warning: To recover we must rescan the entire library."
msgstr ""
"Êtes-vous sûr de vouloir supprimer la sélection de la bibliothèque ?\n"
"\n"
"Attention: Pour récupérer, il faut rescanner toute la bibliothèque."

#: ../src/pragha-menubar.c:190
msgid "_Playback"
msgstr "_Lecture"

#: ../src/pragha-menubar.c:191
msgid "Play_list"
msgstr "_Liste de lecture"

#: ../src/pragha-menubar.c:192
msgid "_View"
msgstr "_Vue"

#: ../src/pragha-menubar.c:193 ../src/pragha-playlist.c:222
msgid "_Tools"
msgstr "_Outils"

#: ../src/pragha-menubar.c:194
msgid "_Help"
msgstr "_Aide"

#: ../src/pragha-menubar.c:195 ../src/pragha-statusicon.c:83
#: ../plugins/notify/pragha-notify-plugin.c:177
msgid "Previous track"
msgstr "Piste précédente"

#: ../src/pragha-menubar.c:197 ../src/pragha-statusicon.c:85
msgid "Play / Pause"
msgstr "Lecture / Pause"

#: ../src/pragha-menubar.c:199 ../src/pragha-statusicon.c:87
msgid "Stop"
msgstr "Arrêter"

#: ../src/pragha-menubar.c:201 ../src/pragha-statusicon.c:89
#: ../plugins/notify/pragha-notify-plugin.c:181
msgid "Next track"
msgstr "Piste suivante"

#: ../src/pragha-menubar.c:203 ../src/pragha-playlist.c:225
#: ../src/pragha-statusicon.c:91
msgid "Edit track information"
msgstr "Editer les étiquettes de la piste"

#: ../src/pragha-menubar.c:205 ../src/pragha-statusicon.c:93
msgid "_Quit"
msgstr "_Quitter"

#: ../src/pragha-menubar.c:207 ../src/pragha-statusicon.c:79
msgid "_Add files"
msgstr "_Ajouter des fichiers"

#: ../src/pragha-menubar.c:208
msgid "Open a media file"
msgstr "Ouvrir un fichier multimédia"

#: ../src/pragha-menubar.c:209 ../src/pragha-statusicon.c:81
msgid "Add _location"
msgstr "Ajouter _emplacement"

#: ../src/pragha-menubar.c:211
msgid "_Add the library"
msgstr "_Ajouter la bibliothèque"

#: ../src/pragha-menubar.c:213
msgid "Remove selection from playlist"
msgstr "Enlever la séléction de la liste de lecture"

#: ../src/pragha-menubar.c:215 ../src/pragha-playlist.c:215
msgid "Crop playlist"
msgstr "Récupérer la liste de lecture"

#: ../src/pragha-menubar.c:217 ../src/pragha-playlist.c:217
msgid "Clear playlist"
msgstr "Effacer la liste de lecture"

#: ../src/pragha-menubar.c:219 ../src/pragha-playlist.c:219
#: ../src/pragha-playlists-mgmt.c:69 ../src/pragha-playlists-mgmt.c:92
#: ../src/pragha-playlists-mgmt.c:1244
msgid "Save playlist"
msgstr "Sauvegarder la liste de lecture"

#: ../src/pragha-menubar.c:220 ../src/pragha-menubar.c:225
#: ../src/pragha-playlists-mgmt.c:1333 ../src/pragha-playlists-mgmt.c:1369
msgid "New playlist"
msgstr "Nouvelle liste de lecture"

#: ../src/pragha-menubar.c:224 ../src/pragha-playlist.c:220
#: ../src/pragha-playlists-mgmt.c:71 ../src/pragha-playlists-mgmt.c:94
#: ../src/pragha-playlists-mgmt.c:1246
msgid "Save selection"
msgstr "Sauvegarder la sélection"

#: ../src/pragha-menubar.c:229
msgid "_Search in playlist"
msgstr "_Chercher dans la liste de lecture"

#: ../src/pragha-menubar.c:231
msgid "_Preferences"
msgstr "_Préférences"

#: ../src/pragha-menubar.c:233
msgid "Jump to playing song"
msgstr "_Aller au morceau en cours de lecture"

#: ../src/pragha-menubar.c:235
msgid "E_qualizer"
msgstr "Egaliseur"

#: ../src/pragha-menubar.c:237
msgid "_Rescan library"
msgstr "_Rescanner la bibliothèque"

#: ../src/pragha-menubar.c:239
#: ../plugins/removable-media/pragha-devices-removable.c:302
msgid "_Update library"
msgstr "_Mise à jour de la bibliothèque"

#: ../src/pragha-menubar.c:241
msgid "_Statistics"
msgstr "_Statistiques"

#: ../src/pragha-menubar.c:243
msgid "Homepage"
msgstr "Site Internet"

#: ../src/pragha-menubar.c:245
msgid "Community"
msgstr "Communauté"

#: ../src/pragha-menubar.c:247
msgid "Wiki"
msgstr "Wiki"

#: ../src/pragha-menubar.c:249
msgid "Translate Pragha"
msgstr "Traduire Pragha"

#: ../src/pragha-menubar.c:251 ../src/pragha-statusicon.c:77
msgid "About"
msgstr "À propos"

#: ../src/pragha-menubar.c:256
msgid "_Shuffle"
msgstr "Lecture a_léatoire"

#: ../src/pragha-menubar.c:259
msgid "_Repeat"
msgstr "Lecture en _boucle"

#: ../src/pragha-menubar.c:262
msgid "_Fullscreen"
msgstr "_Plein écran"

#: ../src/pragha-menubar.c:265
msgid "Lateral _panel"
msgstr "_Panneau latéral"

#: ../src/pragha-menubar.c:268
msgid "Secondary lateral panel"
msgstr "Second panneau latéral"

#: ../src/pragha-menubar.c:271
msgid "Playback controls below"
msgstr "Contrôles de lecture en dessous"

#: ../src/pragha-menubar.c:274
msgid "Menubar"
msgstr ""

#: ../src/pragha-menubar.c:277
msgid "Status bar"
msgstr "Barre d'état"

#: ../src/pragha-menubar.c:638
msgid "Total Tracks:"
msgstr "Pistes :"

#: ../src/pragha-menubar.c:640
msgid "Total Artists:"
msgstr "Artistes :"

#: ../src/pragha-menubar.c:642
msgid "Total Albums:"
msgstr "Albums :"

#: ../src/pragha-menubar.c:645
msgid "Statistics"
msgstr "Statistiques"

#. Create labels
#: ../src/pragha-playlist.c:114 ../src/pragha-playlist.c:3370
#: ../src/pragha-playlist.c:3546 ../src/pragha-statusicon.c:179
#: ../src/pragha-tags-dialog.c:167 ../src/pragha-tags-dialog.c:1077
msgid "Title"
msgstr "Titre"

#: ../src/pragha-playlist.c:118 ../src/pragha-playlist.c:3374
#: ../src/pragha-playlist.c:3550 ../src/pragha-tags-dialog.c:682
msgid "Bitrate"
msgstr "Débit binaire"

#: ../src/pragha-playlist.c:119 ../src/pragha-playlist.c:3375
#: ../src/pragha-playlist.c:3551 ../src/pragha-tags-dialog.c:172
msgid "Year"
msgstr "Année"

#: ../src/pragha-playlist.c:120 ../src/pragha-playlist.c:3376
#: ../src/pragha-playlist.c:3552 ../src/pragha-tags-dialog.c:173
#: ../src/pragha-tags-dialog.c:1097
msgid "Comment"
msgstr "Commentaire"

#. Create labels
#: ../src/pragha-playlist.c:121 ../src/pragha-playlist.c:3377
#: ../src/pragha-playlist.c:3553 ../src/pragha-statusicon.c:182
#: ../src/pragha-tags-dialog.c:681
msgid "Length"
msgstr "Durée"

#: ../src/pragha-playlist.c:122 ../src/pragha-playlist.c:3378
#: ../src/pragha-playlist.c:3554 ../src/pragha-tags-dialog.c:686
msgid "Filename"
msgstr "Nom du fichier"

#: ../src/pragha-playlist.c:123 ../src/pragha-playlist.c:3379
#: ../src/pragha-playlist.c:3555 ../src/pragha-tags-dialog.c:687
msgid "Mimetype"
msgstr "Type MIME"

#: ../src/pragha-playlist.c:211
msgid "Remove from playback queue"
msgstr "Enlever de la liste de lecture"

#: ../src/pragha-playlist.c:213
msgid "Remove from playlist"
msgstr "Enlever de la liste de lecture"

#: ../src/pragha-playlist.c:221
msgid "_Send to"
msgstr "_Envoyer à"

#: ../src/pragha-playlist.c:2501
#, c-format
msgid "Copy \"%i\" to selected track numbers"
msgstr ""

#: ../src/pragha-playlist.c:2507
#, c-format
msgid "Copy \"%s\" to selected titles"
msgstr ""

#: ../src/pragha-playlist.c:2513
#, c-format
msgid "Copy \"%s\" to selected artists"
msgstr ""

#: ../src/pragha-playlist.c:2519
#, c-format
msgid "Copy \"%s\" to selected albums"
msgstr "Copier \"%s\" aux albums sélectionnés"

#: ../src/pragha-playlist.c:2525
#, c-format
msgid "Copy \"%s\" to selected genres"
msgstr "Copier \"%s\" aux genres sélectionnés"

#: ../src/pragha-playlist.c:2531
#, c-format
msgid "Copy \"%i\" to selected years"
msgstr "Copier \"%i\" aux années sélectionnées"

#: ../src/pragha-playlist.c:2537
#, c-format
msgid "Copy \"%s\" to selected comments"
msgstr "Copier \"%s\" aux commentaires sélectionnés"

#. Create the checkmenu items
#: ../src/pragha-playlist.c:3369 ../src/pragha-playlist.c:3545
#: ../src/pragha.c:626
msgid "Track"
msgid_plural "Tracks"
msgstr[0] ""
msgstr[1] "Piste"

#: ../src/pragha-playlist.c:3382
msgid "Clear sort"
msgstr "Annule le tri"

#: ../src/pragha-playlists-mgmt.c:73
msgid "Playlist"
msgstr "Liste de lecture"

#: ../src/pragha-playlists-mgmt.c:86 ../src/pragha-playlists-mgmt.c:518
#: ../src/pragha-playlists-mgmt.c:1240 ../src/pragha-preferences-dialog.c:1277
#: ../src/pragha-tags-dialog.c:150 ../src/pragha-tags-dialog.c:767
#: ../src/pragha.c:409 ../plugins/song-info/pragha-song-info-dialog.c:84
#: ../plugins/tunein/pragha-tunein-plugin.c:228
msgid "_Ok"
msgstr "_Ok"

#: ../src/pragha-playlists-mgmt.c:129
msgid "<b>con_playlist</b> is a reserved playlist name"
msgstr "<b>con_playlist</b> est un nom de liste de lecture réservé"

#: ../src/pragha-playlists-mgmt.c:153
#, c-format
msgid "Do you want to overwrite the playlist: %s ?"
msgstr "Voulez-vous remplacer la liste de lecture: %s ?"

#: ../src/pragha-playlists-mgmt.c:507
msgid "Choose a new name"
msgstr "Choisir un nouveau nom"

#: ../src/pragha-playlists-mgmt.c:556
#, c-format
msgid "Do you want to delete the item: %s ?"
msgstr "Voulez-vous effacer le champ: %s ?"

#: ../src/pragha-playlists-mgmt.c:586
msgid "Export playlist to file"
msgstr "Exporter la liste de lecture dans un fichier"

#: ../src/pragha-playlists-mgmt.c:590
msgid "_Save"
msgstr "_Enregistrer"

#: ../src/pragha-playlists-mgmt.c:1036
#: ../plugins/lastfm/pragha-lastfm-plugin.c:929
#, c-format
msgid "Added %d songs from %d of the imported playlist."
msgstr "%d chansons ajoutées depuis %d de la liste de lecture importée."

#: ../src/pragha-playlists-mgmt.c:1224
msgid "What do you want to do?"
msgstr "Que voulez-vous faire ?"

#: ../src/pragha-playlists-mgmt.c:1226
#, c-format
msgid "Replace the playlist \"%s\""
msgstr "Remplace la liste de lecture \"%s\""

#: ../src/pragha-playlists-mgmt.c:1231
#, c-format
msgid "Add to playlist \"%s\""
msgstr "Ajouter à la liste de lecture \"%s\""

#: ../src/pragha-preferences-dialog.c:192
msgid ""
"Patterns should be of the form:<filename>;<filename>;....\n"
"A maximum of six patterns are allowed.\n"
"Wildcards are not accepted as of now ( patches welcome :-) )."
msgstr ""
"Les motifs doivent être de la forme : <nom du fichier>;<nom du fichier>;...\n"
"Un maximum de six motifs est autorisé.\n"
"Les wildcards ne sont pas encore acceptés (les patchs sont les "
"bienvenus :-) )."

#: ../src/pragha-preferences-dialog.c:215
msgid "Album art pattern"
msgstr "Motif à l'emplacement de la Pochette"

#: ../src/pragha-preferences-dialog.c:523
#: ../src/pragha-preferences-dialog.c:1132
msgid "Start normal"
msgstr "Démarrer normalement"

#: ../src/pragha-preferences-dialog.c:527
#: ../src/pragha-preferences-dialog.c:1133
msgid "Start fullscreen"
msgstr "Démarrer en mode plein écran"

#: ../src/pragha-preferences-dialog.c:531
#: ../src/pragha-preferences-dialog.c:1134
msgid "Start in system tray"
msgstr "Démarer minimisé"

#. Create a folder chooser dialog
#: ../src/pragha-preferences-dialog.c:668
msgid "Select a folder to add to library"
msgstr "Sélectionner un répertoire à ajouter à la bibliothèque"

#: ../src/pragha-preferences-dialog.c:672
#: ../plugins/lastfm/pragha-lastfm-plugin.c:957
msgid "_Open"
msgstr "_Ouvrir"

#: ../src/pragha-preferences-dialog.c:895
#: ../src/pragha-preferences-dialog.c:1302
msgid "Audio"
msgstr "Audio"

#: ../src/pragha-preferences-dialog.c:897
msgid "Audio sink"
msgstr "Pilote audio"

#: ../src/pragha-preferences-dialog.c:901
#: ../src/pragha-preferences-dialog.c:920
#: ../src/pragha-preferences-dialog.c:926
msgid "Restart Required"
msgstr "Redémarrage obligatoire"

#: ../src/pragha-preferences-dialog.c:916
msgid "Audio Device"
msgstr "Périphérique audio"

#: ../src/pragha-preferences-dialog.c:925
msgid "Use software mixer"
msgstr "Utiliser un logiciel de mixage"

#: ../src/pragha-preferences-dialog.c:963
msgid "Can not change directories while they are analyzing."
msgstr "Impossible de changer de dossier durant leur analyse."

#: ../src/pragha-preferences-dialog.c:973
msgid "Folders"
msgstr "Répertoires"

#: ../src/pragha-preferences-dialog.c:992 ../src/pragha.c:210
msgid "_Add"
msgstr "_Ajouter"

#: ../src/pragha-preferences-dialog.c:993
msgid "_Remove"
msgstr "_Supprimer"

#: ../src/pragha-preferences-dialog.c:1009
msgid "Merge folders in the folders estructure view"
msgstr "Fusionner les dossiers dans la vue hiérarchique"

#: ../src/pragha-preferences-dialog.c:1012
msgid "Sort albums by release year"
msgstr "Trier les albums par année de sortie"

#. Labels
#: ../src/pragha-preferences-dialog.c:1055
#: ../src/pragha-preferences-dialog.c:1282
msgid "Appearance"
msgstr "Apparence"

#: ../src/pragha-preferences-dialog.c:1057
msgid "Use system title bar and borders"
msgstr ""

#: ../src/pragha-preferences-dialog.c:1064
msgid "Use small icons on the toolbars"
msgstr ""

#: ../src/pragha-preferences-dialog.c:1067
msgid "Controls"
msgstr "Contrôles"

#: ../src/pragha-preferences-dialog.c:1069
msgid "Show Album art in Panel"
msgstr "Montrer la pochette d'album dans le panneau"

#: ../src/pragha-preferences-dialog.c:1072
msgid "Size of Album art"
msgstr "Taille de la pochette"

#: ../src/pragha-preferences-dialog.c:1077
msgid "Album art file pattern"
msgstr "Motif à l'emplacement de la pochette"

#: ../src/pragha-preferences-dialog.c:1120
msgid "Search"
msgstr "Recherche"

#. Instant search.
#: ../src/pragha-preferences-dialog.c:1122 ../src/pragha-search-entry.c:41
msgid "Search while typing"
msgstr "Recherche lors de la frappe"

#. Aproximate search.
#: ../src/pragha-preferences-dialog.c:1125 ../src/pragha-search-entry.c:48
msgid "Search similar words"
msgstr "Rechercher des mots similaires"

#: ../src/pragha-preferences-dialog.c:1128
msgid "When starting pragha"
msgstr "Lors du lancement de pragha"

#: ../src/pragha-preferences-dialog.c:1131
msgid "Remember last window state"
msgstr "Se rappeler de la disposition de la fenêtre"

#: ../src/pragha-preferences-dialog.c:1137
msgid "Restore last playlist"
msgstr "Restaurer la dernière liste de lecture"

#: ../src/pragha-preferences-dialog.c:1140
msgid "When adding folders"
msgstr "Lors de l'ajout de dossiers"

#: ../src/pragha-preferences-dialog.c:1141 ../src/pragha.c:201
msgid "Add files recursively"
msgstr "Ajouter les fichiers récursivement"

#: ../src/pragha-preferences-dialog.c:1165
#: ../src/pragha-preferences-dialog.c:1317
msgid "Desktop"
msgstr "Bureau"

#: ../src/pragha-preferences-dialog.c:1167
msgid "Show Pragha icon in the notification area"
msgstr "Afficher l'icône de pragha dans la zone de notification"

#: ../src/pragha-preferences-dialog.c:1170
msgid "Minimize Pragha when closing window"
msgstr "Minimiser Pragha lors de la fermeture de la fenêtre"

#: ../src/pragha-preferences-dialog.c:1191
#: ../src/pragha-preferences-dialog.c:1286
msgid "Plugins"
msgstr "Greffons"

#. The main preferences dialog
#: ../src/pragha-preferences-dialog.c:1273
msgid "Preferences"
msgstr "Préférences"

#: ../src/pragha-preferences-dialog.c:1284
msgid "General"
msgstr "Général"

#: ../src/pragha-preferences-dialog.c:1321
msgid "Services"
msgstr "Services"

#: ../src/pragha-preferences-dialog.c:1335
msgid "Preferences of Pragha"
msgstr "Préférences de Pragha"

#: ../src/pragha-scanner.c:93
#, c-format
msgid "%i files analized of %i detected"
msgstr "%i fichiers analysés sur %i détectés"

#: ../src/pragha-scanner.c:99
msgid "Searching files to analyze"
msgstr "Recherche de fichiers à analyser"

#: ../src/pragha-scanner.c:250
msgid "Library scan complete"
msgstr "Scan de la bibliothèque terminé"

#: ../src/pragha-statusicon.c:174 ../src/pragha-toolbar.c:174
#: ../src/pragha-toolbar.c:524
msgid "<b>Not playing</b>"
msgstr "<b>Arrêté</b>"

#: ../src/pragha-tags-dialog.c:171
msgid "Track No"
msgstr "Piste n°"

#: ../src/pragha-tags-dialog.c:174
msgid "File"
msgstr "Fichier"

#. The main edit dialog
#: ../src/pragha-tags-dialog.c:562 ../src/pragha-tags-dialog.c:764
msgid "Details"
msgstr "Détails"

#: ../src/pragha-tags-dialog.c:664
msgid "channel"
msgid_plural "channels"
msgstr[0] "canal"
msgstr[1] "canaux"

#: ../src/pragha-tags-dialog.c:683
msgid "Channels"
msgstr "Chaînes"

#: ../src/pragha-tags-dialog.c:684
msgid "Samplerate"
msgstr "Fréquence d'échantillonnage"

#: ../src/pragha-tags-dialog.c:685
msgid "Folder"
msgstr "Répertoire"

#: ../src/pragha-tags-dialog.c:1070
msgid "Selection to"
msgstr "Sélection vers"

#: ../src/pragha-tags-dialog.c:1108
msgid "Open folder"
msgstr "Ouvrir le répertoire"

#: ../src/pragha-tags-mgmt.c:197
#, c-format
msgid ""
"Do you want to set the track number of ALL of the selected tracks to: %d ?"
msgstr ""
"Désirez-vous modifier le numéro de piste à TOUTES les pistes sélectionnées "
"pour le numéro : %d ?"

#: ../src/pragha-tags-mgmt.c:215
#, c-format
msgid "Do you want to set the title tag of ALL of the selected tracks to: %s ?"
msgstr ""
"Désirez-vous modifier la balise titre de TOUTES les pistes sélectionnées "
"pour : %s ?"

#: ../src/pragha-toolbar.c:150
#, c-format
msgid ""
"%s <small><span weight=\"light\">by</span></small> %s <small><span weight="
"\"light\">in</span></small> %s"
msgstr ""
"%s <small><span weight=\"light\">par</span></small> %s <small><span weight="
"\"light\">de</span></small> %s"

#: ../src/pragha-toolbar.c:155
#: ../plugins/song-info/pragha-song-info-thread-dialog.c:52
#, c-format
msgid "%s <small><span weight=\"light\">by</span></small> %s"
msgstr "%s <small><span weight=\"light\">par</span></small> %s"

#: ../src/pragha-toolbar.c:159
#, c-format
msgid "%s <small><span weight=\"light\">in</span></small> %s"
msgstr "%s <small><span weight=\"light\">de</span></small> %s"

#: ../src/pragha-toolbar.c:809
msgid "Previous Track"
msgstr "Piste précédente"

#: ../src/pragha-toolbar.c:813
msgid "Play / Pause Track"
msgstr "Lecture / Pause de la piste"

#: ../src/pragha-toolbar.c:817
msgid "Stop playback"
msgstr "Arrêter la lecture"

#: ../src/pragha-toolbar.c:821
msgid "Next Track"
msgstr "Piste suivante"

#: ../src/pragha-toolbar.c:853
msgid "Leave Fullscreen"
msgstr "Quitter le mode plein écran"

#: ../src/pragha-toolbar.c:857
msgid "Play songs in a random order"
msgstr "Lire les morceaux en ordre aléatoire"

#: ../src/pragha-toolbar.c:860
msgid "Repeat playback list at the end"
msgstr "Répéter la lecture jusqu'à la fin"

#: ../src/pragha-utils.c:239
msgid "day"
msgid_plural "days"
msgstr[0] ""
msgstr[1] "jour"

#: ../src/pragha-utils.c:486
msgid "Unable to open the browser"
msgstr "Impossible d'ouvrir le navigateur"

#: ../src/pragha-window.c:115
#, c-format
msgid ""
"<b>Error playing current track.</b>\n"
"(%s)\n"
"<b>Reason:</b> %s"
msgstr ""
"<b>Erreur à la lecture de la piste courante</b>⏎\n"
"(%s)⏎\n"
"<b>Raison:</b> %s"

#: ../src/pragha-window.c:118
msgid "_Stop"
msgstr "_Arrêter"

#: ../src/pragha-window.c:119
msgid "_Next"
msgstr "_Suivant"

#: ../src/pragha.c:176
msgid "Select a file to play"
msgstr "Sélectionner un fichier à jouer"

#: ../src/pragha.c:223 ../plugins/lastfm/pragha-lastfm-plugin.c:961
msgid "Supported media"
msgstr "Fichiers pris en charge"

#: ../src/pragha.c:309
msgid "All files"
msgstr "Tous les fichiers"

#: ../src/pragha.c:385
msgid "Enter the URL of an internet radio stream"
msgstr "Entrer l´URL d´un flux radio internet"

#: ../src/pragha.c:392
msgid "Give it a name to save"
msgstr "Nommer pour enregistrer"

#: ../src/pragha.c:405
msgid "Add a location"
msgstr "Ajouter un emplacement"

#: ../src/pragha.c:537
msgid "translator-credits"
msgstr ""
"Raphaël Huck <raphael.huck@gmail.com>\n"
"Olivier Karquel <yellowspoon@free.fr>\n"
"Raphael Tournoy <lexpas@free.fr>\n"
"Laurent Coudeur <laurentc@iol.ie>\n"
"Stéphane Raimbault  <stephane.raimbault@gmail.com>\n"
"Robert-André Mauchin <zebob.m@pengzone.org>\n"
"Quentin Peten <hidden e-mail>"

#. Setup application name and pulseaudio role
#: ../src/pragha.c:705 ../src/pragha.c:937 ../src/pragha.c:1314
#: ../plugins/dlna/pragha-dlna-plugin.c:222
#: ../plugins/dlna/pragha-dlna-plugin.c:224
msgid "Pragha Music Player"
msgstr "Lecture de musique Pragha"

#: ../data/pragha.desktop.in.h:1
msgid "Music Player"
msgstr "Lecteur de musique"

#: ../data/pragha.desktop.in.h:2
msgid "Manage and listen to music"
msgstr "Gérer et écouter de la musique"

#: ../plugins/acoustid/pragha-acoustid-plugin.c:99
#: ../plugins/acoustid/pragha-acoustid-plugin.c:428
msgid "Search tags on AcoustID"
msgstr "Rechercher des mots-clés sur AcoustID"

#: ../plugins/acoustid/pragha-acoustid-plugin.c:261
msgid "AcoustID not found any similar song"
msgstr "AcoustID n'a trouvé aucun titre similaire"

#: ../plugins/cdrom/pragha-cdrom-plugin.c:477
msgid "Audio/Data CD"
msgstr "CD Audio/de données"

#: ../plugins/cdrom/pragha-cdrom-plugin.c:478
msgid "An audio CD was inserted"
msgstr "Un CD audio a été inséré"

#: ../plugins/cdrom/pragha-cdrom-plugin.c:479
#: ../plugins/cdrom/pragha-cdrom-plugin.c:519
#: ../plugins/cdrom/pragha-cdrom-plugin.c:730
msgid "Add Audio _CD"
msgstr "Ajouter le _CD audio"

#: ../plugins/cdrom/pragha-cdrom-plugin.c:622
msgid "Audio CD"
msgstr "Disque audio"

#: ../plugins/cdrom/pragha-cdrom-plugin.c:624
msgid "Audio CD Device"
msgstr "Périphérique CD Audio"

#: ../plugins/cdrom/pragha-cdrom-plugin.c:644
msgid "Connect to CDDB server"
msgstr "Se connecter au serveur CDDB"

#: ../plugins/devices/pragha-device-client.c:76
msgid "Ignore"
msgstr "Ignorer"

#: ../plugins/dlna-renderer/pragha-dlna-renderer-plugin.c:89
#: ../plugins/dlna-renderer/pragha-dlna-renderer-plugin.c:283
msgid "Search music on DLNA server"
msgstr ""

#: ../plugins/dlna-renderer/pragha-dlna-renderer-plugin.c:220
#, c-format
msgid "Music of the %s server was added."
msgstr ""

#: ../plugins/dlna-renderer/pragha-dlna-renderer-plugin.c:225
msgid "Could not find any DLNA server."
msgstr ""

#: ../plugins/lastfm/pragha-lastfm-plugin.c:174
#: ../plugins/lastfm/pragha-lastfm-plugin.c:1549
msgid "_Lastfm"
msgstr "_Lastfm"

#: ../plugins/lastfm/pragha-lastfm-plugin.c:175
#: ../plugins/lastfm/pragha-lastfm-plugin.c:215
msgid "Love track"
msgstr "J'aime cette piste"

#: ../plugins/lastfm/pragha-lastfm-plugin.c:177
#: ../plugins/lastfm/pragha-lastfm-plugin.c:217
msgid "Unlove track"
msgstr "Je n'aime plus cette piste"

#: ../plugins/lastfm/pragha-lastfm-plugin.c:179
#: ../plugins/lastfm/pragha-lastfm-plugin.c:953
msgid "Import a XSPF playlist"
msgstr "Importer une liste de lecture XSPF"

#: ../plugins/lastfm/pragha-lastfm-plugin.c:181
msgid "Add favorites"
msgstr "Ajouter aux favoris"

#: ../plugins/lastfm/pragha-lastfm-plugin.c:183
#: ../plugins/lastfm/pragha-lastfm-plugin.c:219
msgid "Add similar"
msgstr "Ajouter des pistes similaires"

#: ../plugins/lastfm/pragha-lastfm-plugin.c:432
msgid "Unable to establish conection with Last.fm"
msgstr "Impossible d'établir une connexion avec Last.fm"

#: ../plugins/lastfm/pragha-lastfm-plugin.c:617
msgid "Last.fm suggested a tag correction"
msgstr "Last.fm a suggéré la correction de l´étiquette"

#: ../plugins/lastfm/pragha-lastfm-plugin.c:643
msgid "Love song on Last.fm failed."
msgstr "J´aime cette chanson sur Last.fm échoué"

#: ../plugins/lastfm/pragha-lastfm-plugin.c:661
msgid "Unlove song on Last.fm failed."
msgstr "L'action \"retirer des coup de coeur\" sur Last.fm a échoué"

#: ../plugins/lastfm/pragha-lastfm-plugin.c:768
#, c-format
msgid "Added %d tracks of %d suggested from Last.fm"
msgstr "%d pistes ajoutés sur %d suggestions de Last.fm"

#: ../plugins/lastfm/pragha-lastfm-plugin.c:771
#, c-format
msgid "Last.fm doesn't suggest any similar track"
msgstr "Last.fm ne propose aucun titre similaire"

#: ../plugins/lastfm/pragha-lastfm-plugin.c:775
#, c-format
msgid "Added %d songs of the last %d loved on Last.fm."
msgstr "%d pistes ont été ajoutées parmi les %d dernières aimées sur Last.fm."

#: ../plugins/lastfm/pragha-lastfm-plugin.c:778
#, c-format
msgid "You don't have favorite tracks on Last.fm"
msgstr "Vous n'avez pas de morceaux favoris sur Last.fm"

#: ../plugins/lastfm/pragha-lastfm-plugin.c:1248
msgid "Last.fm submission failed"
msgstr "La soumission à Last.fm a échoué"

#: ../plugins/lastfm/pragha-lastfm-plugin.c:1250
msgid "Track scrobbled on Last.fm"
msgstr "Piste soumise à Last.fm"

#: ../plugins/lastfm/pragha-lastfm-plugin.c:1360
msgid "Update current song on Last.fm failed."
msgstr "Mise à jour de la chanson en cours sur Last.fm échouée"

#: ../plugins/lastfm/pragha-lastfm-plugin.c:1787
msgid "Scrobble on Last.fm"
msgstr "Publier sur Last.fm"

#: ../plugins/lastfm/pragha-lastfm-plugin.c:1790
msgid "Username"
msgstr "Identifiant"

#: ../plugins/lastfm/pragha-lastfm-plugin.c:1797
msgid "Password"
msgstr "Mot de passe"

#: ../plugins/mpris2/pragha-mpris2-plugin.c:856
#, fuzzy
msgid "Tracks"
msgstr "Piste n°"

#: ../plugins/notify/pragha-notify-plugin.c:165
#, c-format
msgid "by <b>%s</b> in <b>%s</b> <b>(%s)</b>"
msgstr "par <b>%s</b> dans <b>%s</b> <b>(%s)</b>"

#: ../plugins/notify/pragha-notify-plugin.c:267
msgid "Notifications"
msgstr "Notifications"

#: ../plugins/notify/pragha-notify-plugin.c:269
msgid "Show Album art in notifications"
msgstr "Afficher les pochettes des albums dans les notifications"

#: ../plugins/notify/pragha-notify-plugin.c:274
msgid "Add actions to change track in notifications"
msgstr "Ajouter les actions sur les pistes dans les notifications"

#: ../plugins/removable-media/pragha-devices-removable.c:211
#, c-format
msgid "Unable to access \"%s\""
msgstr "Impossible d'accéder à \"%s\""

#: ../plugins/removable-media/pragha-devices-removable.c:214
#: ../plugins/removable-media/pragha-devices-removable.c:300
msgid "Removable Device"
msgstr "Périphérique amovible"

#: ../plugins/removable-media/pragha-devices-removable.c:297
#, c-format
msgid "Want to manage \"%s\" volume?"
msgstr ""

#: ../plugins/song-info/pragha-song-info-plugin.c:88
msgid "Search _lyric"
msgstr ""

#: ../plugins/song-info/pragha-song-info-plugin.c:90
msgid "Search _artist info"
msgstr ""

#: ../plugins/song-info/pragha-song-info-plugin.c:377
msgid "Song Information"
msgstr "Informations sur la chanson"

#: ../plugins/song-info/pragha-song-info-plugin.c:379
msgid "Download the album art while playing their songs."
msgstr "Télécharger la pochette de l'album pendant la lecture des morceaux."

#: ../plugins/song-info/pragha-song-info-thread-dialog.c:51
#, c-format
msgid "Lyrics thanks to %s"
msgstr "Paroles fournies par %s"

#: ../plugins/song-info/pragha-song-info-thread-dialog.c:58
#, c-format
msgid "Artist info"
msgstr "Informations sur l'artiste"

#: ../plugins/song-info/pragha-song-info-thread-dialog.c:59
#, c-format
msgid "%s <small><span weight=\"light\">thanks to</span></small> %s"
msgstr "%s <small><span weight=\"light\">grâce à</span></small> %s"

#: ../plugins/song-info/pragha-song-info-thread-dialog.c:81
#: ../plugins/song-info/pragha-song-info-thread-pane.c:100
msgid "Lyrics not found."
msgstr "Les paroles n'ont pas été trouvées."

#: ../plugins/song-info/pragha-song-info-thread-dialog.c:84
#: ../plugins/song-info/pragha-song-info-thread-pane.c:104
msgid "Artist information not found."
msgstr "Aucune information disponible sur l'interprète."

#: ../plugins/song-info/pragha-song-info-thread-pane.c:185
#: ../plugins/song-info/pragha-song-info-thread-pane.c:193
msgid "Searching..."
msgstr "Recherche..."

#: ../plugins/tunein/pragha-tunein-plugin.c:92
#: ../plugins/tunein/pragha-tunein-plugin.c:295
msgid "Search radio on TuneIn"
msgstr "Rechercher une radio sur TuneIn"

#: ../plugins/tunein/pragha-tunein-plugin.c:224
#: ../plugins/tunein/pragha-tunein-plugin.c:235
msgid "Search in TuneIn"
msgstr "Rechercher sur TuneIn"

#~ msgid "Highlight rows on current playlist"
#~ msgstr "Surligner les lignes dans la liste de lecture courante"
